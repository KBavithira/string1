package String;

public class BooleanEquals {

	public static void main(String args[]) {
		
		String str1 = new String("BCAS");
		String str2 = new String("Our BCAS");
		String str3 = new String("IT BCAS");
		
		System.out.println("str1 equals to str2:"+str1.contentEquals(str2));
		System.out.println("str1 equals to str3:"+str1.contentEquals(str3));
		System.out.println("str1 equals to welcome:"+str1.contentEquals("welcome"));
		System.out.println("str1 equals to Hello:"+str1.equals("hello"));
		System.out.println("str1 equals to hello:"+str1.equals("hello"));
	  
	}
}
