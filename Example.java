package String;

public class Example {

	public static void main(String[] args) {
		String str = "Welcome to lastIndex";
		char ch = 'c';
		char ch2 = 'l';
		String subStr = "Index";
		
		int model = str.lastIndexOf(ch);
		int model2 = str.lastIndexOf(ch2);
		int modelsubStr = str.lastIndexOf(subStr);
		
		System.out.println(model);
		System.out.println(model2);
		System.out.println(modelsubStr);
	}
}
