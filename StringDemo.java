package String;

public class StringDemo {

	public static void main(String[]args) {
		// method 1
		String msg = "Welcome";
		String campus = " to BCAS";
		System.out.println(msg);
		
		char value = msg.charAt(3);
		System.out.println(value);
		
		System.out.println(msg.charAt(0));
		System.out.println(""+ msg.charAt(3) + msg.charAt(5) + msg.charAt(6));
					
		System.out.println((msg.concat(campus)).concat(" campus"));
		
		String s1 = msg.concat(campus);
		String s2 = s1.concat("campus");
		System.out.println(s2);
		
		//to check the length of the string
		System.out.println("length of string s2 is : " + s2.length());
		
		}
}
