package String;

public class intindexOf2 {
	public static void main(String[] args) {  
		String str = "Java String";
		
		char ch = 'J';
		char ch2 = 'S';
		String subStr = "tri";
		
		int posOfJ = str.indexOf(ch);
		int posOfS = str.indexOf(ch2);
		int posOfSubstr = str.indexOf(subStr);
		
		System.out.println(posOfJ);
		System.out.println(posOfS);
		System.out.println(posOfSubstr);
	   }  
}
